# RR Coach WIP Janky Build

This is a fork of the very cool RR Coach. Below is information on where to find more information!

## Purpose of your 'WIP Janky Build'?
The purpose is to build a complete pipeline that uses AWS resources to build an automated deployment with this as the application that is deployed. Additional modifications include some sort of backend authentication (Cognito?). This is intended to be a test-bed for various items and ideas. It is not a replacement for RR Coach!


# RR Coach - Origin

[Original Repo](https://gitlab.com/spongechameleon/rr-coach/)

A website to guide users through reddit's r/bodyweightfitness [Recommended Routine](https://www.reddit.com/r/bodyweightfitness/wiki/kb/recommended_routine), commonly referred to as the "RR".

## Getting started

Welcome to the repo, please see the READMEs for both the [frontend](https://gitlab.com/spongechameleon/rr-coach/-/tree/master/frontend) and [backend](https://gitlab.com/spongechameleon/rr-coach/-/tree/master/backend) for startup instructions!

## Design documentation

Please see these [diagrams](https://trello.com/c/hXSQsNcG/5-master-flowchart) (hosted on Trello). Start with "Infrastructure" for the big picture!

FYI: We are going with a JAMStack-like approach here, so the frontend and backend are completely de-coupled and start up separately (only JSON is exchanged between them).
